<?php
// $id$

/**
 * Return a list of details about our forum system to tapatalk
 */
function tapatalk_get_config() {
  $args = func_get_args();
  tapatalk_debug('get_config', $args);

  return array(
    'is_open' => TRUE,
    'guest_okay' => TRUE,
    'api_level' => '3',
    'version' => 'dev',
    'disable_search' => variable_get('tapatalk_disable_search', 0),
    'disable_latest' => variable_get('tapatalk_disable_latest', 0),
    //'goto_post' => '',            // TODO: API4
    //'goto_unread' => '',          // TODO: API4
    //'mark_forum' => '',           // TODO: API4
    //'no_refresh_on_post' => '',   // TODO: API4
    //'get_latest_topic' => '',     // TODO: API4
    //'get_id_by_url' => '',        // TODO: API4
  );
}

/**
 * Return a list of forums and child forums to tapatalk
 */
function tapatalk_get_forum() {
  $args = func_get_args();
  tapatalk_debug('get_forum', $args);
  $t_forums = array();

  $forum_vocab = variable_get('tapatalk_forums_vocab', 0);
  $forums = taxonomy_get_tree($forum_vocab);

  //$forums = forum_get_forums($forum_vocab);

  foreach ($forums as $forum) {
    $t_forum = array();

    // Standard details
    $t_forum['forum_id'] = (string)$forum->tid;
    $t_forum['forum_name'] = xmlrpc_base64($forum->name);
    $t_forum['description'] = xmlrpc_base64($forum->description);
    if ($forum->parents[0]) {
      $t_forum['parent_id'] = (string)$forum->parents[0];
    }
    $t_forum['logo_url'] = ''; // TODO
    // not needed
    //$t_forum['url'] = url("forum/$forum->tid", array('absolute' => TRUE));

    $t_forum['is_protected'] = FALSE; // Drupal doesn't do this

    // Authenticated only
    if (user_is_logged_in()) {
      $t_forum['new_post'] = FALSE; // TODO

      $t_forum['can_subscribe'] = TRUE; // TODO ? Only if subscriptions module present?
      $t_forum['is_subscribed'] = FALSE; // TODO ^^
    }

    if (in_array($forum->tid, variable_get('forum_containers', array()))) {
      $t_forum['sub_only'] = TRUE; // TODO ? Only for authenticated users?
    }
    else {
      $t_forum['sub_only'] = FALSE; // TODO ? Only for authenticated users?
    }

    if ($t_forum['parent_id']) {
      $new_forums = array();
      foreach ($t_forums as $temp) {
        if ($temp['forum_id'] == $t_forum['parent_id']) {
          $temp['child'][] = $t_forum;
        }
        $new_forums[] = $temp;
      }
      $t_forums = $new_forums;
    }
    else {
      $t_forums[] = $t_forum;
    }
  }

  return $t_forums;
}

/**
 * Mark forum (or all if no param) as read
 * Taken almost exactly from the advanced_forum module.
 */
function tapatalk_mark_all_as_read($forum_id = NULL) {
  $args = func_get_args();
  tapatalk_debug('mark_all_as_read', $args);

  global $user;

  if ($forum_id) {
    // Delete the current history entries so already visited nodes get updated.
    $sql = "DELETE h
            FROM {history} AS h
              INNER JOIN {term_node} AS tn ON (h.nid = tn.nid)
            WHERE h.uid = %d AND tn.tid = %d";
    db_query($sql, $user->uid, $current_forum_id);

    // Update the history table with all forum nodes newer than the cutoff.
    $sql = "INSERT INTO {history} (uid, nid, timestamp)
            SELECT DISTINCT %d, n.nid, %d
            FROM {node} AS n
              INNER JOIN {term_node} AS tn ON n.nid = tn.nid
              INNER JOIN {node_comment_statistics} AS ncs ON ncs.nid = n.nid
            WHERE (n.created > %d OR ncs.last_comment_timestamp > %d) AND tn.tid = %d";

    $args = array($user->uid, time(), NODE_NEW_LIMIT, NODE_NEW_LIMIT, $forum_id);
    db_query($sql, $args);
  }
  else {
    // We are on the forum overview, requesting all forums be marked read
    $forum_vocabulary_id = variable_get('forum_nav_vocabulary', '');

    // Delete the current history entries so already visited nodes get updated.
    $sql = "DELETE h
            FROM {history} AS h
              INNER JOIN {term_node} AS tn ON (h.nid = tn.nid)
              INNER JOIN {term_data} AS td ON (td.tid = tn.tid)
            WHERE h.uid = %d AND td.vid = %d";
    db_query($sql, $user->uid, $forum_vocabulary_id);

    // Update the history table with all forum nodes newer than the cutoff.
    $sql = "INSERT INTO {history} (uid, nid, timestamp)
            SELECT DISTINCT %d, n.nid, %d
            FROM {node} AS n
              INNER JOIN {term_node} AS tn ON n.nid=tn.nid
              INNER JOIN {node_comment_statistics} AS ncs ON ncs.nid = n.nid
              INNER JOIN {term_data} AS td ON tn.tid = td.tid
            WHERE (n.created > %d OR ncs.last_comment_timestamp > %d) AND td.vid = %d";

    $args = array($user->uid, time(), NODE_NEW_LIMIT, NODE_NEW_LIMIT, $forum_vocabulary_id);

    db_query($sql, $args);
  }

  return array(
    'result' => TRUE,
    'result_text' => xmlrpc_base64(''),
  );
}

/**
 * Allows mobile client to access a protected sub-forum
 *
 * Not implemented
 */
function tapatalk_login_forum($forum_id, $password) {
  $args = func_get_args();
  tapatalk_debug('login_forum', $args);

  return array(
    'result' => TRUE,
    'result_text' => xmlrpc_base64(''),
  );
}


/**
 * Return the forum, topic, post id from a given url
 */
function tapatalk_get_id_by_url($url) {
  $args = func_get_args();
  tapatalk_debug('get_id_by_url', $args);

  return array(
    'forum_id' => '',
    'topic_id' => '',
    'post_id' => '',
  );
}

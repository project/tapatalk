<?php
// $id$

/**
 * Post a new topic
 */
function tapatalk_new_topic($forum_id, $subject, $text_body, $prefix_id = NULL, $attachment_id_array = array()) {
  global $user;
  $moderation_required = 0;

  $args = func_get_args();
  tapatalk_debug('new_topic', $args);
  $node = new stdClass();

  // Now build the node information
  $node->type = 'forum';
  $node->uid = $user->uid;
  $node->name = $user->name;
  $node->status = 1;    // TODO: Moderation stuff here?
  $node->title = $subject;
  $node->body = $text_body;

  $term = taxonomy_get_term($forum_id);
  $node->{$term->name}[]['value'] = $forum_id;

  // TODO
  // Prefix id
  // Attachment id array

  node_save($node);

  return array(
    'result' => TRUE,
    'result_text' => '',
    'topic_id' => $node->nid,
    'state' => $moderation_required
  );
}

function tapatalk_get_topic($forum_id, $start_num, $last_num = 19, $mode = '') {
  global $user;

  $topics = array();

  $args = func_get_args();
  tapatalk_debug('get_topic', $args);

  $term = taxonomy_get_term($forum_id);

  $sql = "
    SELECT n.nid, r.tid, n.title, n.type, n.sticky, u.name, u.uid, n.created AS timestamp, n.comment AS comment_mode, l.last_comment_timestamp, IF(l.last_comment_uid != 0, cu.name, l.last_comment_name) AS last_comment_name, l.last_comment_uid, l.comment_count AS num_comments, f.tid AS forum_tid
    FROM {node_comment_statistics} l
    INNER JOIN {node} n ON n.nid = l.nid
    INNER JOIN {users} cu ON l.last_comment_uid = cu.uid
    INNER JOIN {term_node} r ON n.vid = r.vid
    INNER JOIN {users} u ON n.uid = u.uid
    INNER JOIN {forum} f ON n.vid = f.vid
    WHERE n.status = 1 AND r.tid = %d";

  $sql = db_rewrite_sql($sql);

  $topic_total = db_result(db_query(db_rewrite_sql("
    SELECT COUNT(n.nid)
    FROM {node} n
    INNER JOIN {term_node} r ON n.vid = r.vid AND r.tid = %d WHERE n.status = 1
  "), $forum_id));

  tapatalk_debug('topic_total', $topic_total);

  if ($mode == 'TOP') {
    $sql .= " AND n.sticky = 1";
  }
  elseif ($mode == 'ANN') {
    // Does Drupal evan have announcements?
  }

  //$sql .= " ORDER BY n.created DESC";
  $sql .= " ORDER BY l.last_comment_timestamp DESC";

  $result = db_query_range($sql, $forum_id, $start_num, ($last_num - $start_num));

  while ($topic = db_fetch_array($result)) {
    $topic_details = array();

    if ($user->uid) {
      $history = _forum_user_last_visit($topic['nid']);   //TODO
      $topic_details['new_post'] = TRUE;
    }
    else {
      $topic_details['new_post'] = FALSE;
    }
    $topic_details['forum_id'] = (int)$forum_id;
    $topic_details['forum_name'] = xmlrpc_base64($term->name);
    $topic_details['topic_id'] = (string)$topic['nid'];
    $topic_details['topic_title'] = xmlrpc_base64($topic['title']);
    $topic_details['topic_author_name'] = xmlrpc_base64($topic['name']);
    $topic_details['topic_author_display_name'] = xmlrpc_base64($topic['name']);
    $topic_details['require_prefix'] = FALSE;
    $topic_details['prefixes'] = array();

    $topic_details['last_reply_time'] = xmlrpc_date($topic['last_comment_timestamp']);

    // Now retrieve specific details on this thread.
    $topic_details['reply_number'] = db_result(db_query('SELECT COUNT(cid) FROM {comments} WHERE nid = %d', $topic['nid']));
    $topic_details['new_post'] = (boolean)comment_num_new($topic['nid']);
    $topic_details['view_number'] = _advanced_forum_get_topic_views_count($topic['nid']);
    $topic_details['short_content'] = xmlrpc_base64(tapatalk_short_content($topic['nid']));

    $topics[] = $topic_details;
  }

  return array(
    'total_topic_num' => (integer)$topic_total,
    'forum_id' => (string)$forum_id,
    'forum_name' => xmlrpc_base64($term->name),
    'can_post' => user_access('create forum topics'),
    //'unread_sticky_count' => 0,
    //'unread_announce_count' => 0,
    //'require_prefix' => 0,
    //'icon_urls' => array('url list'),
    //'prefixes' => array(),
    'topics' => $topics,
  );
}

function tapatalk_get_unread_topic($start_num, $last_num) {
  global $user;

  $args = func_get_args();
  tapatalk_debug('get_unread_topic', $args);

  $unread_topics = array();

  if (user_is_logged_in()) {
    tapatalk_debug('get_unread_topic', 'User is logged in');

    // TODO: These queries are very expensive, see http://drupal.org/node/105639
    $sql = "
      SELECT DISTINCT(n.nid), n.title, n.type, n.changed, n.uid, u.name, GREATEST(n.changed, l.last_comment_timestamp) AS last_updated, l.comment_count
      FROM {node} n
      INNER JOIN {node_comment_statistics} l ON n.nid = l.nid
      INNER JOIN {users} u ON n.uid = u.uid
      LEFT JOIN {comments} c ON n.nid = c.nid AND (c.status = %d OR c.status IS NULL)
      LEFT JOIN {forum} f ON n.nid = f.nid
      LEFT JOIN {term_data} td ON td.tid = f.tid
      WHERE n.status = 1 AND (n.uid = %d OR c.uid = %d) AND ((n.changed > %d) OR (l.last_comment_timestamp > %d))
      ORDER BY last_updated DESC";
    $sql = db_rewrite_sql($sql);

    tapatalk_debug('get_unread_topic', 'After db_rewrite_sql');

    $sql_count = "
      SELECT COUNT(DISTINCT(n.nid))
      FROM {node} n
      INNER JOIN {node_comment_statistics} l ON n.nid = l.nid
      LEFT JOIN {comments} c ON n.nid = c.nid AND (c.status = %d OR c.status IS NULL)
      WHERE n.status = 1 AND (n.uid = %d OR c.uid = %d) AND ((n.changed > %d) OR (l.last_comment_timestamp > %d))";
    $sql_count = db_rewrite_sql($sql_count);

    $total_unread = db_result(db_query($sql_count, COMMENT_PUBLISHED, $user->uid, $user->uid, $user->access, $user->access));

    tapatalk_debug('total_unread', $total_unread);

    $result = db_query_range($sql, $start_num, ($last_num - $start_num), COMMENT_PUBLISHED, $user->uid, $user->uid, $user->access, $account->access);
  }

  while ($row = db_fetch_array($result)) {
    $unread_topic = array();

    $unread_topic['forum_id'] = (string)$row['f.tid'];
    $unread_topic['forum_name'] = xmlrpc_base64($row['td.name']);
    $unread_topic['topic_id'] = (string)$row['n.nid'];
    $unread_topic['topic_title'] = xmlrpc_base64(check_plain($row['n.title']));

    $author = user_load(array('uid' => $row['u.uid']));
    $unread_topic['post_author_name'] = xmlrpc_base64($author->name);

    $unread_topics[] = $unread_topic;
  }

  return array(
    'total_topic_num' => (integer)$total_unread,
    'topics' => $unread_topics
  );
}

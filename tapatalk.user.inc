<?php
// $id$

/**
 * Login a user
 */
function tapatalk_login($login_name, $password) {
  global $user;

  $args = func_get_args();
  tapatalk_debug('login', $args);

  if (user_is_blocked($login_name)) {
    return array(FALSE, t('The username %name has not been activated or is blocked.', array('%name' => $login_name)));
  }
  if (drupal_is_denied('user', $login_name)) {
    return array(FALSE, t('The name %name is a reserved username.', array('%name' => $login_name)));
  }

  $account = user_load(array('name' => $login_name, 'pass' => $password, 'status' => 1));
  if ($account && drupal_is_denied('mail', $account->mail)) {
    return array(FALSE, t('The name %name is registered using a reserved e-mail address and therefore could not be logged in.', array('%name' => $account->name)));
  }

  $user = $account;
  $details = array('name' => $login_name, 'pass' => $password);
  user_authenticate_finalize($details);

  if ($user) {
    $login_valid = TRUE;
  }

  // Now check other things
  $can_pm = FALSE;           // TODO: Depends on privatemsg module?
  $can_send_pm = FALSE;
  $can_moderate = FALSE;
  $usergroup_id = 0;         // Drupal Role maybe?
  $max_attachment = 0;       // Check Upload module?
  $max_png_size = 0;
  $max_jpg_size = 0;

  // Finally return real result
  return array(
    'result' => TRUE,
    'result_text' => xmlrpc_base64(''),
    'can_pm' => $can_pm,
    'can_send_pm' => $can_send_pm,
    'can_moderate' => $can_moderate,
    'usergroup_id' => $usergroup_id,
    'max_attachment' => $max_attachment,
    'max_png_size' => $max_png_size,
    'max_jpg_size' => $max_jpg_size
  );
}

/**
 * Logout a user
 */
function tapatalk_logout_user() {
  global $user;

  $args = func_get_args();
  tapatalk_debug('logout', $args);

  watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

  // Destroy current session:
  session_destroy();
  // Only variables can be passed by reference workaround.
  $null = NULL;
  user_module_invoke('logout', $null, $user);

  // Load the anonymous user
  $user = drupal_anonymous_user();
}

/**
 * Retrieve a list of online users, use the standard drupal settings for online period etc
 */
function tapatalk_get_online_users() {
  $args = func_get_args();
  tapatalk_debug('get_online_users', $args);

  // Count users active within the defined period.
  $interval = time() - variable_get('user_block_seconds_online', 900);

  // Perform database queries to gather online user lists.  We use s.timestamp
  // rather than u.access because it is much faster.
  $anonymous_count = sess_count($interval);
  $authenticated_users = db_query('
    SELECT DISTINCT u.uid, u.name, s.timestamp
    FROM {users} u
    INNER JOIN {sessions} s ON u.uid = s.uid
    WHERE s.timestamp >= %d AND s.uid > 0
    ORDER BY s.timestamp DESC', $interval);

  $authenticated_count = 0;
  $max_users = variable_get('user_block_max_list_count', 10);
  $items = array();
  while ($account = db_fetch_object($authenticated_users)) {
    $user_detail = array();
    if ($max_users > 0) {
      $user_detail['icon_url'] = '';        // TODO:
      $user_detail['user_name'] = xmlrpc_base64($account->name);
      $user_detail['display_name'] = xmlrpc_base64($account->name);
      $user_detail['display_text'] = xmlrpc_base64('');    // TODO: No drupal equivalent?
      $max_users--;
    }
    $authenticated_count++;

    $user_details[] = $user;
  }

  return array(
    'member_count' => $authenticated_count,
    'guest_count' => $anonymous_count,
    'list' => $user_details
  );
}

/**
 * Return information for a specified user
 * Code taken quite a bit from author_pane module
 */
function tapatalk_get_user_info($user_name) {
  $args = func_get_args();
  tapatalk_debug('get_user_info', $args);

  $user_detail = array();

  $account = user_load(array('name' => $user_name));

  if ($account) {
    $user_detail['post_count'] = 0;  // TODO:

    $user_detail['reg_time'] = tapatalk_date_format($account->created);
    $user_detail['display_name'] = xmlrpc_base64($account->name);
    $user_detail['last_activity_time'] = format_interval(time() - $account->access);

    if ((time()-$account->access) < variable_get('user_block_seconds_online', 900)) {
      $user_detail['is_online'] = TRUE;
    }
    else {
      $user_detail['is_online'] = FALSE;
    }
    $user_detail['accept_pm'] = FALSE;              // TODO: privatemsg module
    $user_detail['icon_url'] = '';                  // TODO: author_pane module?
    $user_detail['current_activity'] = xmlrpc_base64('');          // TODO: No drupal equivalent
    $user_detail['custom_fields_list'] = array();   // TODO: profile module? content_profile?
  }

  return $user_detail;
}

/**
 * Return a list of topics (max 50) the user has previously created
 */
function tapatalk_get_user_topic($user_name) {
  $args = func_get_args();
  tapatalk_debug('get_user_topic', $args);

  $user_topics = array();

  $account = user_load(array('name' => $user_name));

  if ($account) {
    // Retrieve list of forum nodes (topics) created
    $result = db_query_range("
      SELECT n.nid, n.title, f.tid, td.name, n.created, nr.body
      FROM {node} n
      LEFT JOIN {node_revisions} nr ON nr.nid = n.nid
      LEFT JOIN {forum} f ON n.nid = f.nid
      LEFT JOIN {term_data} td ON td.tid = f.tid
      WHERE n.type = 'forum' AND n.uid = %d
      ORDER BY n.created DESC
    ", 0, 50, $account->uid);

    $user_topics = tapatalk_build_topic_list($result);
  }

  return $user_topics;
}

/**
 * Return a list of posts that a particular user has replied to
 */
function tapatalk_get_user_reply_post($user_name) {
  $args = func_get_args();
  tapatalk_debug('get_user_replies', $args);

  $user_replies = array();

  $account = user_load(array('name' => $user_name));

  if ($account) {
    // Retrieve list of forum nodes (topics) replied to
    $sql = "
      SELECT n.nid, n.title, f.tid, td.name, n.created, nr.body
      FROM {comments} c
      LEFT JOIN {node} n ON c.nid = n.nid
      LEFT JOIN {node_revisions} nr ON nr.nid = n.nid
      LEFT JOIN {forum} f ON n.nid = f.nid
      LEFT JOIN {term_data} td ON td.tid = f.tid
      WHERE n.type = 'forum' AND c.uid = %d
      ORDER BY c.timestamp DESC";

    $sql = db_rewrite_sql($sql);
    $result = db_query_range($sql, 0, 50, $account->uid);

    $user_topics = tapatalk_build_topic_list($result);
  }

  return $user_topics;
}

function tapatalk_build_topic_list($result) {
  while ($row = db_fetch_array($result)) {
    $user_topic['forum_id'] = (string)$row['f.tid'];
    $user_topic['forum_name'] = xmlrpc_base64($row['td.name']);
    $user_topic['topic_id'] = (string)$row['n.nid'];
    $user_topic['topic_title'] = xmlrpc_base64(check_plain($row['n.title']));

    // Last reply information, could maybe be moved to a sub-query?
    $last_reply_result = db_query_range("SELECT * FROM {comments} WHERE nid = %d ORDER BY timestamp DESC", 0, 1, $row['n.nid']);
    if ($last_reply_result) {
      $last_reply_details = db_fetch_array($last_reply_result);
      $last_author = user_load(array('uid' => $last_reply_details['uid']));
      $user_topic['last_reply_author_name'] = xmlrpc_base64($last_author->name);
      $user_topic['last_reply_author_display_name'] = xmlrpc_base64($last_author->name);
      $user_topic['icon_url'] = '';     // TODO
      $user_topic['last_reply_time'] = tapatalk_date_format($last_reply_details['timestamp']);
      $user_topic['reply_number'] = db_result(db_query("SELECT count(nid) FROM {comments} WHERE nid = %d", $row['n.nid']));
    }

    // Get unmber of views on this topic
    if (module_exists('statistics')) {
      $user_topic['view_number'] = db_result(db_query("SELECT totalcount FROM node_counter WHERE nid = %d", $row['n.nid']));
    }

    $user_topic['short_content'] = xmlrpc_base64(substr(check_plain($row['nr.body']), 0, 200));

    $user_topics[] = $user_topic;
  }

  return $user_topics;
}

<?php
// $id$

/**
 * 
 */
function tapatalk_report_post() {
  $args = func_get_args();
  tapatalk_debug('report_post', $args);
}

/**
 * 
 */
function tapatalk_reply_post() {
  $args = func_get_args();
  tapatalk_debug('reply_post', $args);
}

/**
 * 
 */
function tapatalk_get_quote_post() {
  $args = func_get_args();
  tapatalk_debug('get_quote_post', $args);
}

/**
 * 
 */
function tapatalk_get_raw_post() {
  $args = func_get_args();
  tapatalk_debug('get_raw_post', $args);
}

/**
 * 
 */
function tapatalk_save_raw_post() {
  $args = func_get_args();
  tapatalk_debug('save_raw_post', $args);
}

/**
 * Returns a list of posts under the same thread, given a topic_id
 */
function tapatalk_get_thread($topic_id, $start_num, $last_num, $return_html = FALSE) {
	$args = func_get_args();
  tapatalk_debug('get_thread', $args);

  $node = node_load($topic_id);
  
  if ($node) {
  	$topic_total = 1;
  }
  else {
  	// Log error
  	return;
  }
  
  $topic_id = $node->nid;
  $topic_title = $node->title;
  $term = tapatalk_lookup_forum($node);
  
  $posts = array();
  
  // Put the node into the first "post"
  $temp['post_id'] = $node->nid;
  $temp['post_title'] = xmlrpc_base64(($return_html ? $node->title : decode_entities(strip_tags($node->title))));
  $temp['post_content'] = xmlrpc_base64(($return_html ? $node->body : decode_entities(strip_tags($node->body))));
  $temp['post_author_name'] = xmlrpc_base64($node->owner);
  //$temp['is_online'] = // TODO: lookup sessions table
  //$temp['can_edit'] = // TODO: can the user edit
  $temp['icon_url'] = tapatalk_user_icon_url($node->uid);
  $temp['post_time'] = tapatalk_date_format($node->created);

  $posts[] = $temp;
  
  if (user_access('access comments')) {
    $mode = _comment_get_display_setting('mode', $node);
    $order = _comment_get_display_setting('sort', $node);
    $comments_per_page = _comment_get_display_setting('comments_per_page', $node);

    // Multiple comment view
    $query_count = 'SELECT COUNT(*) FROM {comments} c WHERE c.nid = %d';
    $query = 'SELECT c.cid as cid, c.pid, c.nid, c.subject, c.comment, c.format, c.timestamp, c.name, c.mail, c.homepage, u.uid, u.name AS registered_name, u.signature, u.signature_format, u.picture, u.data, c.thread, c.status FROM {comments} c INNER JOIN {users} u ON c.uid = u.uid WHERE c.nid = %d';

    $query_args = array($node->nid);
    if (!user_access('administer comments')) {
      $query .= ' AND c.status = %d';
      $query_count .= ' AND c.status = %d';
      $query_args[] = COMMENT_PUBLISHED;
    }

    if ($order == COMMENT_ORDER_NEWEST_FIRST) {
      if ($mode == COMMENT_MODE_FLAT_COLLAPSED || $mode == COMMENT_MODE_FLAT_EXPANDED) {
        $query .= ' ORDER BY c.cid DESC';
      }
      else {
        $query .= ' ORDER BY c.thread DESC';
      }
    }
    else if ($order == COMMENT_ORDER_OLDEST_FIRST) {
      if ($mode == COMMENT_MODE_FLAT_COLLAPSED || $mode == COMMENT_MODE_FLAT_EXPANDED) {
        $query .= ' ORDER BY c.cid';
      }
      else {
        // See comment above. Analysis reveals that this doesn't cost too
        // much. It scales much much better than having the whole comment
        // structure.
        $query .= ' ORDER BY SUBSTRING(c.thread, 1, (LENGTH(c.thread) - 1))';
      }
    }
    $query = db_rewrite_sql($query, 'c', 'cid');
    $query_count = db_rewrite_sql($query_count, 'c', 'cid');
      
    // Start a form, for use with comment control.
    $result = pager_query($query, $comments_per_page, 0, $query_count, $query_args);

    $divs = 0;
    $num_rows = FALSE;
    $comments = '';
    while ($comment = db_fetch_object($result)) {
      $comment = drupal_unpack($comment);
      $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
      $comment->depth = count(explode('.', $comment->thread)) - 1;
      
      tapatalk_debug('get_thread', nl2br(print_r($comment, TRUE)));
            
      $topic_total++;
      $temp['post_id'] = $comment->cid;
      $temp['post_title'] = xmlrpc_base64(($return_html ? $comment->subject : decode_entities(strip_tags($comment->subject))));
      $temp['post_content'] = xmlrpc_base64(($return_html ? $comment->comment : decode_entities(strip_tags($comment->comment))));
      $temp['post_author_name'] = xmlrpc_base64($comment->name);
      $temp['icon_url'] = tapatalk_user_icon_url($comment->uid);
      $temp['post_time'] = tapatalk_date_format($comment->created);

      $posts[] = $temp;
    } 	
  	
  }
  
  return array(
    'total_post_num' => (int)$topic_total,
    'forum_id' => (string)$term->tid,
    'forum_name' => xmlrpc_base64($term->name),
    'topic_id' => (string)$topic_id,
    'topic_title' => xmlrpc_base64($topic_title), // TODO
    //'prefix' => 0,
    //'is_subscribed' => 0, // TODO - depend on subscriptions?
    //'can_subscribe' => 0, // TODO - depend on subscriptions?
    'is_closed' => FALSE, // TODO - support thread closing?
    'can_reply' => user_access('create forum topics'), // TODO - support thread closing?
    'posts' => $posts,
  );
}

/**
 * 
 */
function tapatalk_thread_by_unread() {
  $args = func_get_args();
  tapatalk_debug('thread_by_unread', $args);
}

/**
 * 
 */
function tapatalk_thread_by_post() {
	$args = func_get_args();
	tapatalk_debug('thread_by_post', $args);
}
